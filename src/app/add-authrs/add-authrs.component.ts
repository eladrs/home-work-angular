import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthorsService } from './../authors.service';


@Component({
  selector: 'app-add-authrs',
  templateUrl: './add-authrs.component.html',
  styleUrls: ['./add-authrs.component.css']
})
export class AddAuthrsComponent implements OnInit {
  id: number;
  name: string;

  constructor(private router:Router, private authorsservice:AuthorsService) { }

  ngOnInit() {
    
  }
  addAuthor(){
    console.log(this.name);
    this.authorsservice.addAuthors(this.name)
    this.router.navigate(['/authors']);
  }

}

