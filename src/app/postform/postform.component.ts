import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-postform',
  templateUrl: './postform.component.html',
  styleUrls: ['./postform.component.css']
})

export class PostformComponent implements OnInit {
  title :string;
  author :string;
  body :string;
  id :string;
  isEdit:boolean = false;
  buttonText:string = 'Add post'
  headerText:string = 'New post'

  constructor(private postsservice:PostsService
    ,private router:Router
    ,private route: ActivatedRoute) { }

    onSubmit(){ 
      if(this.isEdit){
        console.log('edit mode');
        this.postsservice.updatePost(this.id,this.title,this.body,this.author);
      }
      else {
       this.postsservice.addPost(this.title,this.body,this.author)
      }
      this.router.navigate(['/posts']);
    }  

    ngOnInit() {
      this.id = this.route.snapshot.params.id;
      if(this.id) {
      this.isEdit = true;
      this.buttonText = 'Update post'
      this.headerText = 'Edit post'
      this.postsservice.getPost(this.id).subscribe(
        post => {
          this.title = post.data().title;
          this.body = post.data().body; 
          this.author = post.data().author;
        }
     )
     }
    }

}
