import { Post } from './../interfaces/posts';
import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import {Router} from '@angular/router';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  panelOpenState = false;
  posts$: Observable<Post[]>;
 
  
  constructor(private postsService:PostsService,private router: Router) { }

  ngOnInit() {
    this.posts$= this.postsService.getPosts();
    
  }

  addPostsToFirestore(){
    this.postsService.addToFirestore();
    this.router.navigate(['/posts']);
  }

  //deletePost = (postId: string) => {
  //this.postsService.deletePost(postId);
  //}
  deletePost(id: string){
    this.postsService.deletePost(id)
 }
}
