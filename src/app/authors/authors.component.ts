import { AuthorsService } from './../authors.service';
import { Component, OnInit } from '@angular/core';
//import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';


export interface Authors{
  id: number;
  name: string;
}

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})

export class AuthorsComponent implements OnInit {//name,RenameAuthor,id פה בעצם יש לי  3משתנים 
  id:number;
  name: string;
  // Authors:any;
  authors$:Observable<any>;
  
  /*authors: Authors[] = [
            {id: 1, name: 'Lewis Carrol'},
            {id: 2, name: 'Leo Tolstoy'},
            {id: 3, name: 'Thomas Mann'}];
            
constructor(private router: Router, private route:ActivatedRoute) { };
*/

constructor(private authorsservice:AuthorsService) { }// dependency injection הדבר הזה זה בעצם 


  ngOnInit() {
    /*this.id = this.route.snapshot.params['id'];// url אני בעצם לוקח תמונת מצב רגעית עבור הפרמטרים שמופיעים ב 
    this.RenameAuthor = this.route.snapshot.params['author'];//לכאורה כאן אני מצפה שהפרמטר של שם הכותב ישתנה 
    this.authors.forEach(author => {
      if(author.id == this.id){ 
        author.name = this.RenameAuthor;
      }
    });
    */
  this.authors$ = this.authorsservice.getAuthors();
  }
}
