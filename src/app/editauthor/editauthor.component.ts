
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-editauthor',
  templateUrl: './editauthor.component.html',
  styleUrls: ['./editauthor.component.css']
})
export class EditauthorComponent implements OnInit {
  id: number;
  author:string;

  constructor(private router:Router, private route:ActivatedRoute) { }

  ngOnInit() {
    this.author = this.route.snapshot.params['author'];
    this.id = this.route.snapshot.params['id'];
  }
  onSubmit(){
    this.router.navigate(['/authors',this.id,this.author]);
  }
}
