
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestore,AngularFirestoreModule} from '@angular/fire/firestore';
import { AngularFireAuthModule,AngularFireAuth } from '@angular/fire/auth';




import { NavComponent } from './nav/nav.component';
import { BooksComponent } from './books/books.component';
import { AuthorsComponent } from './authors/authors.component';
import { EditauthorComponent } from './editauthor/editauthor.component';
import { AddAuthrsComponent } from './add-authrs/add-authrs.component';
import { PostsComponent } from './posts/posts.component';
import { AuthorsService } from './authors.service';
import { PostsService } from './posts.service';
import { PostformComponent } from './postform/postform.component';
import { AuthComponent } from './auth/auth.component';

const appRoutes: Routes = [
  { path: 'books', component: BooksComponent },//urlמגדיר את ה path
  { path: 'add-authrs', component: AddAuthrsComponent },
  { path: 'authors', component: AuthorsComponent},//מגדיר את השימוש באיזה קומפננט אני משתמש component
  { path: 'authors/:id/:author', component: AuthorsComponent},
  { path: 'editauthor/:id/:author/edit', component: EditauthorComponent},
  { path: 'posts', component: PostsComponent },
  { path: 'postform', component: PostformComponent },
  { path: 'postform/:id', component: PostformComponent},
  {path: 'auth',component: AuthComponent},
  { path: '',
    redirectTo: '/books',//books הערך הדיפולטיבי אם לא הזנתי ניתוב ספציפי אגיע לדף שמציין את ה 
    pathMatch: 'full'
  },
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    BooksComponent,
    AuthorsComponent,
    EditauthorComponent,
    AddAuthrsComponent,
    PostsComponent,
    PostformComponent,
    AuthComponent
 ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    MatCardModule, 
    FormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    HttpClientModule,
    
    RouterModule.forRoot(
      appRoutes,// { enableTracing: true } // <-- debugging purposes only
    ), 
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireModule,
    AngularFireModule.initializeApp(environment.firebase)
  ],

  providers: [AuthorsService,PostsService,AngularFirestore,AngularFireAuth],
  bootstrap: [AppComponent]
})
export class AppModule { }
