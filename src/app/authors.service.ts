import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


// @Injectable({
//   providedIn: 'root'
// })

export class AuthorsService {
  authors:any =[{id:1, name:'Lewis Carrol'}
  ,{id:2, name:'Leo Tolstoy'},
  {id:3, name:'Thomas Mann'}];

//Observable פה אני יוצר   
getAuthors(){
  const authorsObservable= new Observable(// Observableמסוג authorsObservable הגדרת משתנה בשם 
    observer=>{//, Observableמייצג את המידע שיוחזר מ2 השורות למטה ,זה בעצם משתנה ש"נרשם" בכדי לקבל מידע מה
      setInterval(
        ()=>observer.next(this.authors),1000//כאן אני מציג מה המידע שאני רוצה שיחזור אלי 
      )
    }
  )
  return authorsObservable;
}
addAuthors(name: string){
  console.log(name);
    this.authors.push({id:this.authors.length+1,name:name})
  }
  
  constructor() { }
}
