
import { HttpClient } from '@angular/common/http';
import { Post } from './interfaces/posts';
import { User } from './interfaces/users';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map} from 'rxjs/operators';//Observable שזו הספרייה של  rxjs מספריית  map יבוא של האופרטור 
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class PostsService {

private POSTURL = "https://jsonplaceholder.typicode.com/posts/";// (JSONPlaceholder) הנתון  api כאן אנו משתמשים ב 
private USERURL = "https://jsonplaceholder.typicode.com/users";
private itemsCollection: AngularFirestoreCollection<Post>;
private posts: Observable<Post[]>;

constructor(private http: HttpClient,private db:AngularFirestore){
  this.itemsCollection = db.collection<Post>('posts');
}
    // HttpClient זה הדרך של אנגולר לתקשר בעזרת אג'קס עם צד השרת ,שרת מסוג 

    getData(): Observable<Post[]>{
    this.posts = this.http.get<Post[]>(`${this.POSTURL}`).pipe(
    map((data) => this.addUsersToPosts(data)));
    return this.posts;    
    }

    getInfo():Observable<User[]>{
    const users = this.http.get<User[]>(`${this.USERURL}`);
    return users;
    }

    getPosts():Observable<Post[]>{//doc של כל  id בשיטה הזאת אני יכול לקבל את ה 
    this.posts=this.itemsCollection.valueChanges({idField:'id'});//doc של כל  id פה אני בעצם מקבל את ה 
    return this.posts; 
    } 

    getPost(id:string):Observable<any>{
      return this.db.doc(`posts/${id}`).get();
    }
              
    addPost(title:string,body:string, author:string){
     const post= {author:author,title:title,body:body}
     this.db.collection('posts').add(post);
    }
    
    updatePost(id:string,title:string,body:string,author:string){
      this.db.doc(`posts/${id}`).update(
      {
      title:title,
      body:body,
      author:author
      })
       } 
       
    
    deletePost(id:string){
    console.log("This is the postID: "+id)
     this.db.doc(`posts/${id}`).delete();
    }   


addUsersToPosts(data: Post[]): Post[]{
  const users = this.getInfo();
  const postsArray = []
  users.forEach(user => {    
      user.forEach(u =>{
          data.forEach(post =>{
              if(post.userId === u.id){
                  postsArray.push({
                      id: post.id,
                      userId:post.userId,
                      title: post.title,
                      body: post.body,
                      userName: u.name
                  })
              }
          })
      })
  })
return postsArray;
}
                 
addToFirestore(){
           const users = this.getInfo();
            const posts = this.http.get<Post[]>(`${this.POSTURL}`);
            let items: Observable<Post[]>;
            items = this.itemsCollection.valueChanges();
    
            items.forEach(data => {
                if(data.length === 0){
                   posts.forEach(post =>{
                       post.forEach(postEl => {
                           users.forEach(user => {
                                user.forEach(el => {
                                   if(el.id === postEl.userId){        
                                       postEl.userName = el.name;
                                        this.db.collection("posts").add({
                                            title: postEl.title,
                                            body: postEl.body,
                                            author: postEl.userName
                                        })
                                    }
                                })
                            })
                        })
                    })
                }
               else{
                    console.log("all posts were added!")
                }
            })
         }

     
}
