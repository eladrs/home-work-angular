
import { AuthService } from './auth.service';
import { Component} from '@angular/core';
import { NgForm} from '@angular/forms'
import { Auth } from './../interfaces/auth';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})

export class AuthComponent  {
isLoginMode=false;
error:string=null;

constructor(private authService :AuthService, private router:Router) { }

  onSwitchMode(){
    this.isLoginMode=!this.isLoginMode 
  }

  onSubmit(form:NgForm){
    if(!form.valid){return;}//אם המשתמש הצליח לעקוף את חוקי הולידציה אז הוספנו פה חוק נוסף שנועד למנוע שליחת טופס שגוי
    
    const email = form.value.email;
    const password = form.value.password;
    let authObs: Observable <Auth>
    if(this.isLoginMode){
      authObs=this.authService.login(email,password);
    }else{
      authObs=this.authService.signup(email,password);
    }
    authObs.subscribe(
      resData=>{
        console.log(resData)
        this.router.navigate(['/posts'])
        },
        errorMessage=>{
        console.log(errorMessage);  
        this.error= errorMessage;
        }
      );
    form.reset();//כדי שהשדות ישארו ריקים לאחר שליחת הטופס 
      }
  }

