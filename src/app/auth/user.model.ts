export class User{
    constructor(
        public email:string,
        public id: string,
        private _token:string,
        private _tokenExpirationDate: Date,// פג תוקף לאחר שעה  token בגלל ש
    ){} 

    get token(){//token בכדי לגשת לפרטים של ה 
       if(!this._tokenExpirationDate || new Date()>this._tokenExpirationDate){//כבר לא רלוונטי  token זה אומר שה 
        return null;
    }
        return this._token;
    }
}