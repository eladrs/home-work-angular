import { AuthService } from './../auth/auth.service';
import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, Subscription } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Location } from "@angular/common";
import { Router } from "@angular/router";
import { Auth } from '../interfaces/auth';


@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  private userSub: Subscription;
  isAuthenticated= false;

  title: string = 'Books';
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver, location: Location, router: Router
    ,private AuthService: AuthService){
    router.events.subscribe(val => {
      if (location.path() == "/books") {
        this.title = 'Books';
        console.log(this.title)
      } else if (location.path() == "/authors") {
        this.title = "Authors";
      } else if (location.path() == "/posts")
        this.title = "Posts";
      else 
      this.title="Authentication"

    });   
  }
  ngOnInit(){
    this.userSub=this.AuthService.user.subscribe(user=>{//בשיטה הנ"ל אנו בעצם בודקים האם המשתמש מחובר
      this.isAuthenticated=!user? false : true;
      console.log(!user)
    });
  }
  ngOnDestroy(){
    this.userSub.unsubscribe();
  }
  onLogout(){
    this.AuthService.logout();
  }

}
