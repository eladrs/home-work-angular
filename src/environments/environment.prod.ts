export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCGLJNtj5OoBuDbCbc9X10g-ALZqjHDxjk",
    authDomain: "home-work-angular.firebaseapp.com",
    databaseURL: "https://home-work-angular.firebaseio.com",
    projectId: "home-work-angular",
    storageBucket: "home-work-angular.appspot.com",
    messagingSenderId: "913607125470",
    appId: "1:913607125470:web:216810d73eca542da4554f",
    measurementId: "G-MPEHPKRTJ0"
  }
};
